$(document).ready(function() {

    function myPush() {
        var pushValue = "-75%";
        var pushValueReset = "0%";

        if ($('.navbar-toggler').children('i').hasClass('fa-bars')) {
            $('.pushMenu').css("right","0%");
            $('#main').css("left",pushValue);
            $('.mynav').css("left",pushValue);
            $('.navbar-toggler').children('i').removeClass('fa-bars');
            $('.navbar-toggler').children('i').addClass('fa-times');
        } else if ($('.navbar-toggler').children('i').hasClass('fa-times')) {
            $('.pushMenu').css("right","-100%");
            $('#main').css("left",pushValueReset);
            $('.mynav').css("left",pushValueReset);
            $('.navbar-toggler').children('i').removeClass('fa-times');
            $('.navbar-toggler').children('i').addClass('fa-bars');
        } else {
            console.log("Error in myPush Function");
        }
    }

    $('.navbar-toggler, .pushMenu a.nav-link').click(function() { myPush(); }); 

});