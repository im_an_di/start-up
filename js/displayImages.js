$(document).ready(function() { 
    $("#main").scroll(function() { 
        if($("#main").scrollTop() >= 170 && $("#main").scrollTop() <= 300) {
            $('#coffee').animate({
                marginTop: "0px",
                opacity: "1"
            }, "slow");
        } else if($("#main").scrollTop() >= 417 && $("#main").scrollTop() <= 500) {
            $('#colombia').animate({
                marginTop: "0px",
                opacity: "1"
            }, "slow");
        } else if($("#main").scrollTop() >= 501 && $("#main").scrollTop() <= 590) {
            $('#adventurous').animate({
                marginTop: "0px",
                opacity: "1"
            }, "slow");
        }
    });
});