$(document).ready(function() {

    $('form[id="contactForm"]').validate({
        rules: {
            name: {
                required: true,
                minlength: 2
            },
            mail: {
                required: true,
                email: true
            },
            phone: {
                required: true,
                number: true
            }

        }, 
        messages: {
            name: {
                required: '¡No olvides llenar este espacio!',
                minlength: '¿Cuál es tu nombre?'
            },
            mail: {
                required: '¡No olvides llenar este espacio!',
                email: '¿Cuál es tu email?'
            },
            phone: {
                required: '¡No olvides llenar este espacio!',
                number: '¿Cuál es tu número de teléfono?'
            }
        },
        submitHandler: function() {
            $.ajax({
                url: "contactForm.php",
                type: "post",
                data: $('form[id="contactForm"]').serialize(),
                success: function(response) {
                    $('.form-group:last-child').before('<div class="form-group" id="formMessage"><span class="text-success">'+ response +'</span></div>');
                    $('#contactForm').find('input, textarea').val('');
                    resetMessage(true);
                }            
            });
        }
    });

    function resetMessage(state) {
        if (state != false) {
            setTimeout(function() {
                $('#formMessage').remove();
            }, 3000);
        }
    }
 });