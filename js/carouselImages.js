$(document).ready(function() {
    $.ajax({
        url: 'js/carouselData.json',
        dataType: 'json',
        type: 'get',
        success: function(data) {
            var response = '', indicator = '';
            for (var i = 0; i < data.items.length; i++) {
                indicator += '<li data-target="#carouselIndicators" data-slide-to="'+i+'"></li>';
                response += '<div class="carousel-item"><img class="d-block w-100" src="' + data.items[i].image + '" alt="' + data.items[i].name + '" ></div>';
            }
            $('.carousel-inner').append(response);
            $('.carousel-indicators').append(indicator);
            $('.carousel-item').first().addClass('active');
            $('.carousel-indicators > li').first().addClass('active');
        }
    });
});